package com.techu.apitechuv2.controllers;

import org.junit.Assert;
import org.junit.Test;

public class HelloControllersTest {

    @Test
    public void testHello() {
        HelloController sut = new HelloController();

        Assert.assertEquals("Hola Pilar!", sut.hello("Pilar"));
    }
}
