package com.techu.apitechuv2.controllers;

import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.util.BsonUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/apitechu/v2")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET , RequestMethod.POST})
public class ProductController {

    @Autowired
    ProductService productService;
    private ProductModel putProductById;
    private Object ProductModel;



    @GetMapping("/products")
    public List<ProductModel> getProducts() {
        System.out.println("getProducts");

        return this.productService.findAll();

    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id) {
        System.out.println("getProductById");
        System.out.println("La id del producto a buscar es " + id);

        Optional<ProductModel> result = this.productService.findById(id);

        if (result.isPresent() == true) {
            return new ResponseEntity<>(result.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);

        }

    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product) {
        System.out.println("addProduct");
        System.out.println("La id del producto que se va a crear es " + product.getId());
        System.out.println("La descripción del producto que se va a crear es " + product.getDesc());
        System.out.println("El precio del producto que se va a crear es " + product.getPrice());

        return new ResponseEntity<>(this.productService.add(product), HttpStatus.CREATED);
    }

    // mi práctica

    @PutMapping ("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("updateProduct");
        System.out.println("La id del producto a modificar es " + id);
        System.out.println("La descripción del producto a actualizar" + product.getDesc());
        System.out.println("El precio del producto a actualizar" + product.getPrice());

         Optional<ProductModel> productToUpdate =this.productService.findById(id);

//  if (this.productService.findById(id).isPresent() == true){
//  si lo declaras arriba acortas codigo

           if (productToUpdate.isPresent() == true) {
            System.out.println("Producto para actualizar encontraado, actualizando");
            this.productService.update(product);
        }

        return new ResponseEntity<>(product,
                productToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);

    }
    @DeleteMapping ("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id)  {
        System.out.println("deleteProduct");
        System.out.println("La ide del prodcuto a borrar es" + id);

        boolean deteleProduct = this.productService.delete(id);

        return new ResponseEntity<>(
                deteleProduct ? "Producto borrado" : "Producto no borrado",
                deteleProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }


    }


