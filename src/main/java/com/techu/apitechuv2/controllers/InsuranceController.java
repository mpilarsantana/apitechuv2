package com.techu.apitechuv2.controllers;

import com.techu.apitechuv2.models.InsuranceModel;
import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.services.InsuranceService;
import com.techu.apitechuv2.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.util.BsonUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/apitechu/v2")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET , RequestMethod.POST})
public class InsuranceController {

    @Autowired
    InsuranceService insuranceService;

    @GetMapping("/insurances")
    public List<InsuranceModel> getInsurances() {
        System.out.println("getInsurances");

        return this.insuranceService.findAll();

    }

    @GetMapping("/insurances/{id}")
    public ResponseEntity<Object> getInsuranceById(@PathVariable String id) {
        System.out.println("getInsuranceById");
        System.out.println("La id del seguro a buscar es " + id);

        Optional<InsuranceModel> result = this.insuranceService.findById(id);

        if (result.isPresent() == true) {
            return new ResponseEntity<>(result.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Seguro no encontrado", HttpStatus.NOT_FOUND);

        }

    }

    @PostMapping("/insurances")
    public ResponseEntity<InsuranceModel> addInsurance(@RequestBody InsuranceModel insurance) {
        System.out.println("addInsurance");
        System.out.println("La id del seguro que se va a crear es " + insurance.getId());
        System.out.println("El nombre del seguro que se va a crear es " + insurance.getName());
        System.out.println("La descripción del seguro que se va a crear es " + insurance.getDesc());
        System.out.println("El precio del seguro que se va a crear es " + insurance.getPrice());
        System.out.println("La ruta del seguro que se va crear es " + insurance.getRute());

        return new ResponseEntity<>(this.insuranceService.add(insurance), HttpStatus.CREATED);
    }
}