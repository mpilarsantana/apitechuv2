package com.techu.apitechuv2.services;


import com.techu.apitechuv2.models.InsuranceModel;
import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.repositories.InsuranceRepository;
import com.techu.apitechuv2.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class InsuranceService {

    @Autowired
    InsuranceRepository insuranceRepository;

    public List<InsuranceModel> findAll(){

        return this.insuranceRepository.findAll();

    }

    public Optional<InsuranceModel> findById(String id) {
        System.out.println("findById");
        System.out.println("Obteniendo el seguro con la id " + id);

        return this.insuranceRepository.findById(id);

    }

    public InsuranceModel add(InsuranceModel insurance) {
        System.out.println("add");

        return this.insuranceRepository.save(insurance);
    }

    public InsuranceModel update (InsuranceModel insurance) {
        System.out.println("update");

        return this.insuranceRepository.save(insurance);
    }

    public boolean delete(String id) {
        System.out.println("delete");
        boolean result = false;

        if (this.insuranceRepository.findById(id).isPresent() == true) {
            System.out.println("Producto encontrado, borrando");
            this.insuranceRepository.deleteById(id);
            result = true;
        }

        return result;
    }

}
