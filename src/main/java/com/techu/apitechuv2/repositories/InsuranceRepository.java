package com.techu.apitechuv2.repositories;


import com.techu.apitechuv2.models.InsuranceModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InsuranceRepository extends MongoRepository<InsuranceModel , String> {
}
