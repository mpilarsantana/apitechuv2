package com.techu.apitechuv2.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "insurance")
public class InsuranceModel {

    @Id
    private String id;
    private String name;
    private String desc;
    private String rute;
    private float price;

    public InsuranceModel() {
    }

    public InsuranceModel(String id, String name, String desc, String rute, float price) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.rute = rute;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getRute() {
        return rute;
    }

    public void setRute(String rute) {
        this.rute = rute;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}